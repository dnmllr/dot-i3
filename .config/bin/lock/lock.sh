#!/bin/bash

fscrn=$(mktemp --tmpdir XXXX.png)


maim $fscrn

gm mogrify -scale 10% -scale 1000% $fscrn

revert() {
        xset dpms 0 0 0
}

trap revert SIGHUP SIGINT SIGTERM
xset +dpms dpms 5 5 5
i3lock -ni $fscrn

revert
rm $fscrn
